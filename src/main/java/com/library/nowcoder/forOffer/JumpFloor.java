package com.library.nowcoder.forOffer;

import java.util.Scanner;

/**
 * <p>项目名称: nowcoder </p> 
 * <p>文件名称: JumpFloorII </p> 
 * <p>类型描述: [一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶总共有多少种跳法。] </p>
 * @author idea
 * @date 2020年1月11日
 */
public class JumpFloor {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int target = scanner.nextInt();
        System.out.println(JumpFloor.JumpFloorII(target));
        scanner.close();
    }

    /**
     * <p>功能描述: [分析:f(n) = f(n-1) + f(n-2) + ... + f(0)
     * f(n-1) = f(n-2) + f(n-3) + ... + f(0)==>f(n)=2*f(n-1)] </p>
     * @Title JumpFloorII
     * @return
     */
    public static int JumpFloorII(int target) {
        if (target == 1) {
            return target;
        }
        if (target <= 0) {
            return 0;
        }
        int res = 1;
        for (int i = 1; i < target; i++) {
            res = 2 * res;
        }
        return res;
    }
}
