package com.library.nowcoder.forOffer;

/**
 * <p>项目名称: nowcoder </p> 
 * <p>文件名称: FibonacciSequence </p> 
 * <p>类型描述: [大家都知道斐波那契数列，现在要求输入一个整数n，请你输出斐波那契数列的第n项（从0开始，第0项为0）。 n<=39 ] </p>
 * @author lifanfan
 * @date 2020年1月2日
 */
public class FibonacciSequence {

    public static void main(String[] args) {
        System.out.println(FibonacciSequence.getNum(2));
    }

    public static long getNum(int n) {
        if (n <= 1) {
            return n;
        }
        return getNum(n - 1) + getNum(n - 2);
    }

}
