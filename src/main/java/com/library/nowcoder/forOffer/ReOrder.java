package com.library.nowcoder.forOffer;

import com.alibaba.fastjson.JSONObject;

/**
 * <p>项目名称: nowcoder </p> 
 * <p>文件名称: ReOrder </p> 
 * <p>类型描述: [输入一个整数数组，实现一个函数来调整该数组中数字的顺序，
 * 使得所有的奇数位于数组的前半部分，所有的偶数位于数组的后半部分，
 * 并保证奇数和奇数，偶数和偶数之间的相对位置不变。 ] </p>
 * @author idea
 * @date 2020年1月11日
 */
public class ReOrder {
    public static void main(String[] args) {
        int[] array = {1, 3, 5, 7, 2, 4, 6};
        // reOrderArray2(array);
        System.out.println(Sum_Solution(-2));
    }

    public static void reOrderArray(int[] array) {
        if (array == null || array.length == 0) {
            return;
        }
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (!isEven(array[j]) && isEven(array[j + 1])) {
                    swap(array, j, j + 1);
                }
            }
        }
        System.out.println(JSONObject.toJSON(array));
    }

    public static void reOrderArray2(int[] array) {
        if (array == null || array.length == 0) {
            return;
        }
        // 奇数个数
        int oddCnt = 0;
        for (int x : array)
            if (isEven(x))
                oddCnt++;
        int[] copy = array.clone();
        int i = 0, j = oddCnt;
        for (int num : copy) {
            if (num % 2 == 1)
                array[i++] = num;
            else
                array[j++] = num;
        }
        System.out.println(JSONObject.toJSON(array));
    }

    /**
     * <p>功能描述: [判断是否为奇数] </p>
     * @Title isEven
     * @param num
     * @return
     */
    private static boolean isEven(Integer num) {
        return num % 2 != 0;
    }

    /**
     * <p>功能描述: [交换位置 ] </p>
     * @Title swap
     * @param nums
     * @param i
     * @param j
     */
    private static void swap(int nums[], int i, int j) {
        int t = nums[i];
        nums[i] = nums[j];
        nums[j] = t;
    }

    public static int Sum_Solution(int n) {
        int sum = n;
        boolean b = (n > 0) && ((sum += Sum_Solution(n - 1)) > 0);
        return sum;
    }
}
