package com.library.nowcoder.school.netEase;

import java.util.Scanner;

/**
 * <p>
 * 项目名称: nowcoder
 * </p>
 * <p>
 * 文件名称: DivideByThree
 * </p>
 * <p> 类型描述: [小Q得到一个神奇的数列: 1, 12,123,...12345678910,1234567891011...。 
 * *并且小Q对于能否被3整除这个性质很感兴趣。小Q现在希望你能帮他计算一下从数列的第l个到第r个(包含端点)有多少个数可以被3整除。 ]</p>
 * 
 * @author lifanfan
 * @date 2019/12/27
 */
public class DivideByThree {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long s = in.nextLong();
        long e = in.nextLong();
        System.out.println(DivideByThree.getNumber(s, e));
        in.close();
    }

    /**
     * <p>
     * 功能描述: [找到规律后的优化,规律:余数1,0,0,1,0,0...循环,此方式未用到循环]
     * </p>
     * 
     * @Title getNumber
     * @param s
     * @param e
     * @return
     */
    public static long getNumber(long s, long e) {
        long result = 0;
        long exs = s % 3 == 2 ? 1 : 0;
        long exe = e % 3 == 2 ? 1 : 0;
        long res = (s / 3) * 2 + exs;// 获取s前面一共有多少个数可以被三整除(包含s)
        long ree = (e / 3) * 2 + exe;// 获取e前面一共有多少个数可以被三整除(包含e)
        if (s % 3 != 1) {
            result = ree - res + 1;
        } else {
            result = ree - res;
        }
        return result;
    }

    /**
     * <p>
     * 功能描述: [根据观察发现位置除以三余数为一时该位置上的数不能被三整除,其他都可以]
     * </p>
     * 
     * @param s
     * @param e
     * @return
     */
    public static long getNumber2(long s, long e) {
        long result = 0;
        if (s <= e) {
            for (long i = s; i <= e; i++) {
                if (i % 3 != 1) {
                    result++;
                }
            }
        } else {
            for (long i = s; i <= e; i++) {
                if (i % 3 != 1) {
                    result--;
                }
            }
        }
        return result;
    }

    /**
     * <p>
     * 功能描述: [该位置上所有数之和可以被3整除即可,但其实求得不一定是该位置上数之和,但是不影响结果]
     * </p>
     * 
     * @param s
     * @param e
     * @return
     */
    public static long getNumber3(long s, long e) {
        long result = 0;
        if (s <= e) {
            for (long i = s; i <= e; i++) {
                if ((i + 1) * i / 2 % 3 == 0) {
                    result++;
                }
            }
        } else {
            for (long i = e; i <= s; i++) {
                if ((i + 1) * i / 2 % 3 == 0) {
                    result--;
                }
            }
        }
        return result;
    }
    // 其他一次循环的跟上面相比也要复杂一点,最粗暴的两次循环就不考虑
}
