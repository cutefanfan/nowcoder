package com.library.nowcoder.school.netEase;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
* <p>项目名称: nowcoder </p> 
* <p>文件名称: Streetlight </p> 
* <p>类型描述: [小Q正在给一条长度为n的道路设计路灯安置方案。为了让问题更简单,小Q把道路视为n个方格,需要照亮的地方用'.'表示, 不需要照亮的障碍物格子用'X'表示。
* *小Q现在要在道路上设置一些路灯, 对于安置在pos位置的路灯, 这盏路灯可以照亮pos - 1, pos, pos + 1这三个位置。
* *小Q希望能安置尽量少的路灯照亮所有'.'区域, 希望你能帮他计算一下最少需要多少盏路灯。 ] </p>
* @author lifanfan
* @date 2019/12/30
*/
public class Streetlight {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int test = in.nextInt();
        List<Integer> list = new ArrayList<Integer>();
        for (int i = test; i > 0; i--) {
            int length = in.nextInt();
            String str = in.next();
            // System.out.println(Streetlight.getLights(length, str));
            list.add(Streetlight.getLights(length, str));
        }
        for (Integer num : list) {
            System.out.println(num);
        }
        // System.out.println("结束");
        in.close();
    }

    public static int getLights(int length, String str) {
        char[] chars = str.toCharArray();
        int i = 0;
        int count = 0;
        while (i < length) {
            if (String.valueOf(chars[i]).equals(".")) {
                i += 3;
                count++;
            } else {
                i += 1;
            }
        }
        return count;
    }
}
