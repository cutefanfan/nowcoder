package com.library.nowcoder.school.huawei;

import java.util.Scanner;

import org.apache.commons.lang3.StringUtils;

/**
 * <p>项目名称: nowcoder </p> 
 * <p>文件名称: StringSplit </p> 
 * <p>类型描述: [ •连续输入字符串，请按长度为8拆分每个字符串后输出到新的字符串数组； 
 * •长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。每个字符串长度小于100 ] </p>
 * @author idea
 * @date 2020年1月13日
 */
public class StringSplit {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        String str2 = sc.nextLine();
        sc.close();
        String str = StringSplit.changeTo8N(str1) + StringSplit.changeTo8N(str2);
        // List<String> list = new ArrayList<String>();
        for (int i = 0; i < str.length() / 8; i++) {
            System.out.println(str.substring(8 * i, 8 * (i + 1)));
            // list.add(str.substring(8 * i, 8 * (i + 1)));
        }
        // System.out.println(list.toString());
    }

    public static String changeTo8N(String str) {
        if (StringUtils.isEmpty(str)) {
            return "";
        }
        int num = str.length() % 8;
        if (num == 0) {
            return str;
        }
        StringBuffer sb = new StringBuffer();
        sb.append(str);
        for (int i = 0; i < 8 - num; i++) {
            sb.append("0");
        }
        return sb.toString();
    }
}
