package com.library.nowcoder.school.huawei;

import java.util.Scanner;

/**
 *<p>项目名称: library </p> 
 * <p>文件名称: LengthOfLastWord </p> 
 * <p>类型描述: [给定一个仅包含大小写字母和空格 ' ' 的字符串，返回其最后一个单词的长度。如果不存在最后一个单词，请返回 0 。 ] </p>
 * @author idea
 * @date 2019/12/25
 */
public class LengthOfLastWord {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        System.out.println(LengthOfLastWord.lengthOfLastWord(s));
    }

    public static int lengthOfLastWord(String s) {
        if (s.trim().length() <= 0) {
            return 0;
        }
        String[] strings = s.trim().split(" ");
        return strings[strings.length - 1].length();
    }
}
