package com.library.nowcoder.school.huawei;

import java.util.Arrays;
import java.util.Scanner;

/**
 * <p>项目名称: nowcoder </p> 
 * <p>文件名称: RandomOfMing </p> 
 * <p>类型描述: [明明想在学校中请一些同学一起做一项问卷调查，为了实验的客观性，他先用计算机生成了N个1到1000之间的随机整数（N≤1000），
 * 对于其中重复的数字，只保留一个，把其余相同的数去掉，不同的数对应着不同的学生的学号。然后再把这些数从小到大排序，按照排好的顺序去找同学做调查。
 * 请你协助明明完成“去重”与“排序”的工作(同一个测试用例里可能会有多组数据，希望大家能正确处理)。] </p>
 * @author idea
 * @date 2020年1月13日
 */
public class RandomOfMing {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int n = sc.nextInt();
            int[] intArr = new int[n];
            for (int i = 0; i < n; i++) {
                intArr[i] = sc.nextInt();
            }
            RandomOfMing.getNum(intArr);
            // sc.close();
            // System.exit(0);
        }
    }

    public static void getNum(int[] array) {
        Arrays.sort(array);
        for (int i = 0; i < array.length; i++) {
            // 第一个数字或者不等于前一个数字都可以输出
            if (i == 0 || array[i] != array[i - 1]) {
                System.out.println(array[i]);
            }
        }
    }
}
