package com.library.nowcoder.school.huawei;

import java.util.Scanner;

/**
* <p>项目名称: nowcoder </p> 
* <p>文件名称: NumberBaseConversion </p> 
* <p>类型描述: [基础题干:写出一个程序，接受一个十六进制的数(字符串格式)，输出该数值的十进制(字符串格式)表示。（多组同时输入 ）] </p>
* @author lifanfan
* @date 2020年1月14日
*/
public class RadixConversion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("请输入目标进制：");
        long jinzhi = scanner.nextLong();
        System.out.print("请输入要转换的数字：");
        long num = scanner.nextLong();
        scanner.close();
        System.out.println(RadixConversion.decimalToOthers(jinzhi, num));

        // Scanner sc = new Scanner(System.in);
        // while (sc.hasNext()) {
        // String str = sc.next().substring(2);16十六进制数一般以0x开头,所以如此取值
        // System.out.println(Long.parseLong(str, 16));
        // }
        // System.out.println(Integer.parseInt("A", 16));
    }

    // 十进制转其他进制
    public static String decimalToOthers(long n, long num) {
        // n 表示目标进制, num 要转换的值
        String str = "";
        long yushu; // 保存余数
        long shang = num; // 保存商
        while (shang > 0) {
            yushu = shang % n;
            shang = shang / n;

            // 10-15 -> a-f
            if (yushu > 9) {
                str = (char)('a' + (yushu - 10)) + str;
            } else {
                str = yushu + str;
            }
        }
        return str;
    }

    // 十进制字符串加法
}
