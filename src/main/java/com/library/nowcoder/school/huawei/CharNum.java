package com.library.nowcoder.school.huawei;

import java.util.Scanner;

/**
 * <p>项目名称: nowcoder </p> 
 * <p>文件名称: CharNum </p> 
 * <p>类型描述: [ 写出一个程序，接受一个由字母和数字组成的字符串，和一个字符，然后输出输入字符串中含有该字符的个数。不区分大小写。] </p>
 * @author idea
 * @date 2020年1月3日
 */
public class CharNum {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine().toLowerCase();
        String s = sc.nextLine().toLowerCase();
        System.out.print(CharNum.getCharNum2(str, s));
    }

    public static int getCharNum(String str, String s) {
        return str.length() - str.replaceAll(s, "").length();
    }

    public static int getCharNum2(String str, String s) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == s.charAt(0)) {
                count++;
            }
        }
        return count;
    }
}
